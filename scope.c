

#include <cstdlib>
#include <cassert>

#include "qtypes.h"
#include "quakedef.h"
#include "fs.h"
#include "cvar.h"
//#include "sys.h"
#include "vid.h"

#include "scope.h"
#include <vector>
#include <set>
#include <unordered_set>
#include <algorithm>

#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include "dpsoftrast.h"

#define DPSOFTRAST_DEPTHSCALE (1024.0f*1048576.0f)
#define DPSOFTRAST_DEPTHOFFSET (128.0f)
#define DPSOFTRAST_BGRA8_FROM_RGBA32F(r,g,b,a) (((int)(r * 255.0f + 0.5f) << 16) | ((int)(g * 255.0f + 0.5f) << 8) | (int)(b * 255.0f + 0.5f) | ((int)(a * 255.0f + 0.5f) << 24))
#define DPSOFTRAST_DEPTH32_FROM_DEPTH32F(d) ((int)(DPSOFTRAST_DEPTHSCALE * (1-d)))

static unsigned int frameCounter = 0;
const int MAX_LINES = 10000;

int frameLineCount;
double avgFrameLineCount = 0.0;
std::unordered_set<uint64_t>* tempEdgeBuffer[8]; // used by dpsoftrast

// Create a string with last error message
std::string GetLastErrorStdStr()
{
	DWORD error = GetLastError();
	if (error)
	{
		LPVOID lpMsgBuf;
		DWORD bufLen = FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			error,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&lpMsgBuf,
			0, NULL);
		if (bufLen)
		{
			LPCSTR lpMsgStr = (LPCSTR)lpMsgBuf;
			std::string result(lpMsgStr, lpMsgStr + bufLen);

			LocalFree(lpMsgBuf);

			return result;
		}
	}
	return std::string();
}


std::vector<SCOPE_Line> linebuffer;
HANDLE thePipe = INVALID_HANDLE_VALUE;

void connect_pipe()
{
	thePipe = CreateFile("\\\\.\\pipe\\lines", GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (thePipe == INVALID_HANDLE_VALUE) {
		auto err = GetLastErrorStdStr();
		return;
	}

	puts("Pipe connected.");
}

void SCOPE_Initialize()
{
	for (int i = 0; i < 8; i++) {
		tempEdgeBuffer[i] = new std::unordered_set<uint64_t>();
		tempEdgeBuffer[i]->reserve(200);
	}

	linebuffer.resize(MAX_LINES);
	connect_pipe();
}

void SCOPE_BeginFrame()
{
	frameLineCount = 0;

	if (thePipe == INVALID_HANDLE_VALUE) {
		connect_pipe();
	}
}

double inline __declspec (naked) __fastcall fast_sqrt(double n)
{
	_asm fld qword ptr[esp + 4]
		_asm fsqrt
	_asm ret 8
}

void depthCullLines()
{
	int width = -1;
	int height = -1;
	unsigned int* depthbuf = DPSOFTRAST_GetDepthBuffer(&width, &height);

	float fwidth = (float)width;
	float fheight = (float)height;

	int ix, iy;

	for (int i = 0; i < frameLineCount; i++) {
		auto& line = linebuffer[i];
		line.visible = true;

		float ddx = line.points[1].x - line.points[0].x;
		float ddy = line.points[1].y - line.points[0].y;
		float ddz = line.points[1].z - line.points[0].z;
		float length = fast_sqrt(ddx*ddx + ddy*ddy);

		if (length < 0.01f) {
			line.visible = false;
			continue;
		}

		int taps = max(1, min(10, length*10));
		float part = 1.0f / taps;

		float x, y, z;
		int taps_visible = 0;

		for (int t=0; t < taps; t++) {
			float progress = t * part;
			//float iprogress = (1.0f - progress);

			x = line.points[0].screen_x + ddx * progress;
			if (x < 0.0f || x > fwidth) {
				goto skip;
			}

			y = line.points[0].screen_y + ddy * progress;
			if (y < 0.0f || y > fheight) {
				goto skip;
			}

			z = line.points[0].z + ddz * progress;

			ix = (int)x;
			iy = (int)y;

			int zz = *(depthbuf + iy * width + ix);

			if (zz == 0)
				goto skip;

			float bz = DPSOFTRAST_DEPTHSCALE / (float)zz;
			bz += 0.2f; // depth bias;

			if (bz > z) {
				taps_visible++;
			}

			continue;
			skip:
			continue;
		}

		if (taps_visible == 0) {
			line.visible = false;
		}
	}
}

void SCOPE_EndFrame()
{
	//finalLines.clear();
	depthCullLines();

	if (thePipe != INVALID_HANDLE_VALUE && frameLineCount > 0)
	{
		int screenReady = -1;
		int bytesRead = -1;
		int bytesAvailable = -1;
		int dummy;

		PeekNamedPipe(thePipe, &dummy, sizeof(dummy), (LPDWORD)&bytesRead, (LPDWORD)&bytesAvailable, NULL);

		if (bytesAvailable > 0) {
			int readSuccess = ReadFile(thePipe, &screenReady, sizeof(int), (LPDWORD)&bytesRead, NULL);

			if (readSuccess && screenReady) {
				int written = -1;
				int lines = min(frameLineCount, 4000);
				//finalLines.reserve(frameLineCount);
				bool writeSuccess = WriteFile(thePipe, &lines, sizeof(lines), (LPDWORD)&written, NULL);
				// assume tightly packed lines
				writeSuccess = WriteFile(thePipe, &linebuffer[0], lines*sizeof(SCOPE_Line), (LPDWORD)&written, NULL);
				printf("sent %d B of lines. success: %d\n", written, writeSuccess);
				FlushFileBuffers(thePipe);
			}
		}
	}
	//auto err = GetLastError();
	//std::string er = GetLastErrorStdStr();
	//assert(writeSuccess);

	avgFrameLineCount = ((double)frameLineCount + (avgFrameLineCount * frameCounter)) / ((double)frameCounter + 1.0);
	//avgFrameLineCount += frameLineCount/(double)frameCounter;
	printf("FFRAME %u,\t lines = %d/%d\t avg %f\n", frameCounter, frameLineCount, 0, avgFrameLineCount);
	frameCounter++;
}

SCOPE_Line_t* SCOPE_GetBuffer(int** out_countPointer, int* out_maxLines)
{
	*out_countPointer = &frameLineCount;
	*out_maxLines = linebuffer.size();
	
	return &linebuffer[0];
}

std::unordered_set<uint64_t>* SCOPE_GetEdgeBuffer(int index)
{
	return tempEdgeBuffer[index];
}