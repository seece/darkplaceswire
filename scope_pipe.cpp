#include "scope_pipe.h"

#include <cstdlib>

#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <memory>

#include "scope.h"
#define strcat strcat
#define strcpy strcpy
#define strncat strncat
#define strncpy strncpy

