Darkplaces engine hacked to capture geometry as lines. See the original article at http://www.lofibucket.com/articles/oscilloscope_quake.html

This doesn't include the audio generator part.

Only compiles on Windows with Visual Studio 2013. Use the
`darkplaces-wgl-vs2012.vcxproj` project file.

The edge lines are captured in `DPSOFTRAST_Interpret_Draw` function in
`dpsoftrast.c`:
https://bitbucket.org/seece/darkplaceswire/src/7f227fe6d88c7f72c325669ad6eea56368db3bd3/dpsoftrast.c?at=master#cl-4903

The lines are written to a named pipe as an array of structs in `scope.c`:
https://bitbucket.org/seece/darkplaceswire/src/7f227fe6d88c7f72c325669ad6eea56368db3bd3/scope.c?at=master

First the number of lines is written as `int32_t` and then all the lines
as an array. The lines are sent *only* if 1 is read first from the
pipe, as this is a crude way to synchronise the communication between
the two processes.

You could use e.g. network sockets to pass the line data to another process, instead of a name pipe.

You need to use software rendering. Enable it from the Quake console by setting `vid_soft 1` and then issuing command `vid_restart`. 
I also added some custom cvars like `vid_soft_cull_z` and `vid_soft_no_fill`.

