#ifndef SCOPE_H
#define SCOPE_H 

#include <unordered_set>
#include <cstdint>

typedef struct SCOPE_Point {
	float x;
	float y;
	float z;
	float screen_x;
	float screen_y;
} SCOPE_Point_t;

typedef struct SCOPE_Line {
	SCOPE_Point points[2];
	bool visible; // set in the z buffer sampling loop in depthCullLines()

	/*
	bool operator==(const SCOPE_Line& other)
	{
		return (points[0].x == other.points[0].x)
			&& (points[0].y == other.points[0].y)
			&& (points[1].x == other.points[1].x)
			&& (points[1].y == other.points[1].y);
	}
	*/
	friend bool operator==(const SCOPE_Line& a, const SCOPE_Line& b);
} SCOPE_Line_t;


void SCOPE_BeginFrame();
void SCOPE_EndFrame();
void SCOPE_Initialize();
SCOPE_Line_t* SCOPE_GetBuffer(int** out_countPointer, int* out_maxLines);
std::unordered_set<uint64_t>* SCOPE_GetEdgeBuffer(int index);
#endif
